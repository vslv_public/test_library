<?php

namespace vslv;

class CheckService
{
    public static function check(InputData $inputData)
    {
        $inputString = $inputData->getInputString();
        $stack = [];
        for ($i = 0; $i < strlen($inputString); $i++) {
            $current = $inputString[$i];
            if ($current == " " || $current == "\n" || $current == "\t" || $current == "\r") continue;
            if ($current == '(') {
                $stack[] = $current;
            } elseif ($current == ')') {
                $last = array_pop($stack);
                if ($last != '(') return false;
            } else {
                throw new \InvalidArgumentException($current);
            }
        }
        return count($stack) == 0;
    }
}