<?php

namespace vslv;

class InputData
{
    private $inputString;

    public function __construct($inputString)
    {
        $this->inputString = $inputString;
    }

    /**
     * @return mixed
     */
    public function getInputString()
    {
        return $this->inputString;
    }

    /**
     * @param mixed $inputString
     */
    public function setInputString($inputString)
    {
        $this->inputString = $inputString;
    }


}